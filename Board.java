import java.util.Random;
public class Board {
    private Tile[][] board;
    private final static int SIDE_LENGTH = 5;

    public Board() {
        this.board = new Tile[Board.SIDE_LENGTH][Board.SIDE_LENGTH];
        for (int x = 0; x < Board.SIDE_LENGTH; x++) {
            for (int y = 0; y < Board.SIDE_LENGTH; y++) {
                this.board[x][y] = Tile.BLANK;
            }
        }

        Random rng= new Random();
        for (int y=0;y<this.board.length;y++){
             this.board[y][rng.nextInt(this.board[y].length)]= Tile.HIDDEN_WALL_TILE;
        }
    }

    public String toString() {
        String output = "";
        for (Tile[] y : this.board) {
            for (Tile x : y) {
                output += " " + x.getName();
            }
            output += "\n";
        }
        return output;
    }

    // this func doesnt need to validate the input anymore since another func was
    // made to do that,
    // but i kept it this way for the sake of assignment
    public int placeToken(int row, int col) {
        if (!(row >= 0 && row < Board.SIDE_LENGTH && col >= 0 && col < Board.SIDE_LENGTH))
            return -2;
        Tile chosenPosition=this.board[row][col];
        if (chosenPosition== Tile.CASTLE || chosenPosition==Tile.WALL)
            return -1;
        else if (chosenPosition== Tile.HIDDEN_WALL_TILE){
            this.board[row][col]= Tile.WALL;
            return 1;
        }
        else {
            this.board[row][col] = Tile.CASTLE;
            return 0;
        }
    }

    public boolean checkIfFull() {
        for (Tile[] row : this.board) {
            for (Tile col : row) {
                if (col == Tile.BLANK)
                    return false;
            }
        }
        return true;
    }

    public boolean checkIfPositionisFull(int row, int col) {
        return this.board[row][col] != Tile.BLANK;
    }

    public static int getBoardLength() {
        return SIDE_LENGTH;
    }
}
