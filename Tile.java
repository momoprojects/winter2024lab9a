enum Tile {
    BLANK("-"),
    WALL("W"),
    HIDDEN_WALL_TILE("x"),
    CASTLE("C");

    private final String name;

    private Tile(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return getName();
    }
}