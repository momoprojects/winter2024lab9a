import java.util.Scanner;
import java.util.Arrays;
public class BoardGameApp{
    private  static int numOfCastles=5;
    private  static int turns=8;
    private  static Board gameBoard= new Board();
    public static void main(String[] args){
        System.out.println("Welcome to this board game! The rules are simple:");
        System.out.println("\nThere is a "+Board.getBoardLength()+"x"+Board.getBoardLength()+" with a hidden wall tile on each row");
        System.out.println("you have "+turns+" turns to place down your "+numOfCastles+" castle tiles. If you place down a castle on a hidden wall tile, you lose your turn and that wall is revealed.");

        while (turns>0 && numOfCastles>0){
            System.out.println("Number of turns left: "+turns);
            System.out.println("Number of castles left: "+numOfCastles);
            System.out.println(gameBoard);
            processPlayerAnswer();
            System.out.println(gameBoard);
            turns--;
        }
        System.out.println("Final board: \n"+gameBoard);
        if (numOfCastles==0){
            System.out.println("Congratulations! You won");
        }
        else System.out.println("You lost :(");
    }
    public static int processPlayerAnswer(){
        Scanner gameScanner= new Scanner(System.in);
        System.out.println("Choose a column, then a row:");
        System.out.println("Column:");
        int chosenCol= Integer.parseInt(gameScanner.nextLine());
        System.out.println("Row:");
        int chosenRow= Integer.parseInt(gameScanner.nextLine());
        int placeTokenOutcome = gameBoard.placeToken(chosenRow-1,chosenCol-1);
        if (placeTokenOutcome==-2){
            System.out.println("\nChoose a column between 1-"+gameBoard.getBoardLength()+" and a row between 1-"+gameBoard.getBoardLength()+".");
            return processPlayerAnswer();
        }
        else if (placeTokenOutcome==-1){
            System.out.println("\nThere is already a tile there.");
            return processPlayerAnswer();
        }
        else if (placeTokenOutcome==1){
            System.out.println("\nYou placed a castle tile on a hidden wall!");
            return 1;
        }
        else{ 
            numOfCastles--;
            return 1;
            
        }
    }
}
